package fakta.ayu.aplikasi0b

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_jenis.*


class AdapterDataJenis(val dataProdi: List<HashMap<String,String>>,
                       val jenisActivity: JenisActivity
) : //new
    RecyclerView.Adapter<AdapterDataJenis.HolderDataProdi>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterDataJenis.HolderDataProdi {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_jenis,p0,false)
        return HolderDataProdi(v)
    }

    override fun getItemCount(): Int {
        return dataProdi.size
    }

    override fun onBindViewHolder(p0: AdapterDataJenis.HolderDataProdi, p1: Int) {
        val data = dataProdi.get(p1)
        p0.txIdProdi.setText(data.get("id_prodi"))
        p0.txNamaProdi.setText(data.get("nama_prodi"))

        //beginNew
        if(p1.rem(2) == 0) p0.cLayout2.setBackgroundColor(
            Color.rgb(230,245,240))
        else p0.cLayout2.setBackgroundColor(Color.rgb(255,255,245))

        p0.cLayout2.setOnClickListener(View.OnClickListener {
            jenisActivity.edIdJenis.setText(data.get("id_prodi"))
            jenisActivity.edNamaJenis.setText(data.get("nama_prodi"))
        })
        //endNew
    }

    class HolderDataProdi(v: View) : RecyclerView.ViewHolder(v){
        val txIdProdi = v.findViewById<TextView>(R.id.txIdJenis)
        val txNamaProdi = v.findViewById<TextView>(R.id.txNamaJenis)
        val cLayout2 = v.findViewById<ConstraintLayout>(R.id.cLayout2) //new
    }
}
