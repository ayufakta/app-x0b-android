package fakta.ayu.aplikasi0b

import android.Manifest
import android.net.Uri
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_masker.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import kotlinx.android.synthetic.main.row_masker.*
import java.lang.Exception



class MaskerActivity: AppCompatActivity(), View.OnClickListener {

    lateinit var mediaHelper: MediaHelper
    lateinit var namaAdapter: AdapterDataMasker
    lateinit var jenisAdapter: ArrayAdapter<String>
    lateinit var beratAdapter: ArrayAdapter<String>
    var daftarNama = mutableListOf<HashMap<String, String>>()
    var daftarJenis = mutableListOf<String>()
    var daftarBerat = mutableListOf<String>()
    var url = "http://192.168.43.239/masker/show_data.php"
    var url2 = "http://192.168.43.239/masker/get_jenis.php"
    var url3 = "http://192.168.43.239/masker/query_upd_del_ins.php"
    var url4 = "http://192.168.43.239/masker/get_berat.php"
    var imStr = ""
    var pilihJenis = ""
    var pilihBerat = ""
    var namafile = ""
    var fileUri = Uri.parse("")


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_masker)

        try {
            val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
            m.invoke(null)
        } catch (e: Exception) {
            e.printStackTrace()
        }


        namaAdapter = AdapterDataMasker(daftarNama, this) //new
        mediaHelper = MediaHelper()
        listMasker.layoutManager = LinearLayoutManager(this)
        listMasker.adapter = namaAdapter

        jenisAdapter = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, daftarJenis)
        spinJenis.adapter = jenisAdapter
        spinJenis.onItemSelectedListener = itemSelected

        beratAdapter = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, daftarBerat)
        spinBerat.adapter = beratAdapter
        spinBerat.onItemSelectedListener = itemSelected1


        imgUpload.setOnClickListener(this)
        btnInsert.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
        btnFind.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        showDataMasker("")
        getNamaJenis()
        getBerat()
    }

    val itemSelected = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spinJenis.setSelection(0)
            pilihJenis = daftarJenis.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihJenis = daftarJenis.get(position)
        }

    }
    val itemSelected1 = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {

            spinBerat.setSelection(0)
            pilihBerat = daftarBerat.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihBerat = daftarBerat.get(position)
        }

    }

    fun requestPermission() = runWithPermissions(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA
    ) {
        fileUri = mediaHelper.getOutputMediaFileUri()
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)
        startActivityForResult(intent, mediaHelper.getRcCamera())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK)
            if (requestCode == mediaHelper.getRcCamera()) {
                imStr = mediaHelper.getBitmapToString(imgUpload, fileUri)
                namafile = mediaHelper.getMyFileName()
            }
    }


    fun queryInsertUpdateDelete(mode : String){
        val request = object : StringRequest(
            Method.POST,url3,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val kode= jsonObject.getString("kode")
                if(kode.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil", Toast.LENGTH_LONG).show()
                    showDataMasker("")
                }
                else{
                    Toast.makeText(this,"Operasi Gagal", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()

                when (mode) {
                    "insert" -> {
                        hm.put("mode", "insert")
                        hm.put("nim", edKode.text.toString())
                        hm.put("nama", edNama.text.toString())
                        hm.put("image", imStr)
                        hm.put("file", namafile)
                        hm.put("nama_prodi", pilihJenis)
                        hm.put("kelamin", pilihBerat)
                        hm.put("alamat", edDeskripsi.text.toString())
                    }
                    "update" -> {
                        hm.put("mode", "update")
                        hm.put("nim", edKode.text.toString())
                        hm.put("nama", edNama.text.toString())
                        hm.put("image", imStr)
                        hm.put("file", namafile)
                        hm.put("nama_prodi", pilihJenis)
                        hm.put("kelamin", pilihBerat)
                        hm.put("alamat", edDeskripsi.text.toString())
                    }
                    "delete" -> {
                        hm.put("mode", "delete")
                        hm.put("nim", edKode.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun getNamaJenis() {
        val request = StringRequest(
            Request.Method.POST, url2,
            Response.Listener { response ->
                daftarJenis.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length() - 1)) {
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarJenis.add(jsonObject.getString("nama_prodi"))
                }
                jenisAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT)
                    .show()
            }
        )
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun getBerat() {
        val request = StringRequest(
            Request.Method.POST, url4,
            Response.Listener { response ->
                daftarBerat.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length() - 1)) {
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarBerat.add(jsonObject.getString("kelamin"))
                }
                beratAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT)
                    .show()
            }
        )
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun showDataMasker(namaMasker: String) {
        val request = object : StringRequest(
            Request.Method.POST, url,
            Response.Listener { response ->
                daftarNama.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length() - 1)) {
                    val jsonObject = jsonArray.getJSONObject(x)
                    var mhs = HashMap<String, String>()
                    mhs.put("nim", jsonObject.getString("nim"))
                    mhs.put("nama", jsonObject.getString("nama"))
                    mhs.put("nama_prodi", jsonObject.getString("nama_prodi"))
                    mhs.put("url", jsonObject.getString("url"))
                    mhs.put("alamat", jsonObject.getString("alamat"))
                    mhs.put("kelamin", jsonObject.getString("kelamin"))
                    daftarNama.add(mhs)
                }
                namaAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT)
                    .show()
            }) {
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String, String>()
                hm.put("nama", namaMasker)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgUpload -> {
                requestPermission()
            }
            R.id.btnInsert -> {
                queryInsertUpdateDelete("insert")
            }
            R.id.btnUpdate -> {
                queryInsertUpdateDelete("update")
            }
            R.id.btnDelete -> {
                queryInsertUpdateDelete("delete")
            }
            R.id.btnFind -> {
                showDataMasker(edNama.text.toString().trim())
            }
        }
    }
}
