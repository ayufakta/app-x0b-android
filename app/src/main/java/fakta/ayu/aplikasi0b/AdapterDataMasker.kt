package fakta.ayu.aplikasi0b

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_masker.*

class AdapterDataMasker(val dataMasker: List<HashMap<String,String>>,
                        val maskerActivity: MaskerActivity) : //new
    RecyclerView.Adapter<AdapterDataMasker.HolderDataMhs>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): HolderDataMhs {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_masker, p0, false)
        return HolderDataMhs(v)
    }

    override fun getItemCount(): Int {
        return dataMasker.size
    }

    override fun onBindViewHolder(p0: AdapterDataMasker.HolderDataMhs, p1: Int) {
        val data = dataMasker.get(p1)
        p0.txKode.setText(data.get("nim"))
        p0.txNama.setText(data.get("nama"))
        p0.txJenis.setText(data.get("nama_prodi"))
        p0.txDeskripsi.setText(data.get("alamat"))
        p0.txBerat.setText(data.get("kelamin"))

        //beginNew
        if (p1.rem(2) == 0) p0.cLayout.setBackgroundColor(
            Color.rgb(230, 245, 240)
        )
        else p0.cLayout.setBackgroundColor(Color.rgb(255, 255, 245))

        p0.cLayout.setOnClickListener(View.OnClickListener {
            val pos = maskerActivity.daftarJenis.indexOf(data.get("nama_prodi"))
            maskerActivity.spinJenis.setSelection(pos)
            val pos1 = maskerActivity.daftarBerat.indexOf(data.get("kelamin"))
            maskerActivity.spinBerat.setSelection(pos1)
            maskerActivity.edKode.setText(data.get("nim"))
            maskerActivity.edNama.setText(data.get("nama"))
            maskerActivity.edDeskripsi.setText(data.get("alamat"))
            Picasso.get().load(data.get("url")).into(maskerActivity.imgUpload)
        })
        //endNew
        if (!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.photo);
    }

    class HolderDataMhs(v: View) : RecyclerView.ViewHolder(v) {
        val txKode = v.findViewById<TextView>(R.id.txKode)
        val txNama = v.findViewById<TextView>(R.id.txNama)
        val txJenis = v.findViewById<TextView>(R.id.txJenis)
        val photo = v.findViewById<ImageView>(R.id.imageView)
        val txDeskripsi = v.findViewById<TextView>(R.id.txDeskripsi)
        val txBerat = v.findViewById<TextView>(R.id.txBerat)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayout) //new
    }
}
