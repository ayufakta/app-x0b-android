package fakta.ayu.aplikasi0b

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import fakta.ayu.aplikasi0b.AdapterDataJenis
import fakta.ayu.aplikasi0b.R
import kotlinx.android.synthetic.main.activity_jenis.*
import org.json.JSONArray
import org.json.JSONObject


class JenisActivity : AppCompatActivity(), View.OnClickListener {

    //inisiasi variabel
    lateinit var prodiAdapter : AdapterDataJenis
    var daftarProdi = mutableListOf<HashMap<String,String>>()
    var url4 = "http://192.168.43.239/masker/show_data_jenis.php"
    var url5 = "http://192.168.43.239/masker/query_upd_del_ins_jenis.php"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_jenis)
        prodiAdapter = AdapterDataJenis(daftarProdi,this)
        listJenis.layoutManager = LinearLayoutManager(this)
        listJenis.adapter = prodiAdapter
        btnInsJenis.setOnClickListener(this)
        btnUpJenis.setOnClickListener(this)
        btnDelJenis.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        showDataProdi()
    }

    fun queryInsertUpdateDeleteProdi(mode : String){
        val request = object : StringRequest(
            Method.POST,url5,
            Response.Listener { response ->
                Log.i("info","["+response+"]")
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi berhasil", Toast.LENGTH_LONG).show()
                    showDataProdi()
                }else{
                    Toast.makeText(this,"Operasi GAGAL", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when(mode){
                    "insert" ->{
                        hm.put("mode","insert")
                        hm.put("id_prodi",edIdJenis.text.toString())
                        hm.put("nama_prodi",edNamaJenis.text.toString())
                    }
                    "update" ->{
                        hm.put("mode","update")
                        hm.put("id_prodi",edIdJenis.text.toString())
                        hm.put("nama_prodi",edNamaJenis.text.toString())
                    }
                    "delete" ->{
                        hm.put("mode","delete")
                        hm.put("id_prodi",edIdJenis.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun showDataProdi(){
        val request = StringRequest(
            Request.Method.POST,url4,
            Response.Listener { response ->
                daftarProdi.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var prodi = HashMap<String,String>()
                    prodi.put("id_prodi",jsonObject.getString("id_prodi"))
                    prodi.put("nama_prodi",jsonObject.getString("nama_prodi"))
                    daftarProdi.add(prodi)
                }
                prodiAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            })
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }


    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsJenis ->{
                queryInsertUpdateDeleteProdi("insert")
            }
            R.id.btnUpJenis ->{
                queryInsertUpdateDeleteProdi("update")
            }
            R.id.btnDelJenis ->{
                queryInsertUpdateDeleteProdi("delete")
            }
        }
    }

}
