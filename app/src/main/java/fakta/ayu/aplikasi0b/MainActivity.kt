package fakta.ayu.aplikasi0b

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnMasker.setOnClickListener(this)
        btnJenis.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnMasker ->{
                var intent = Intent(this,MaskerActivity::class.java)
                startActivity(intent)
            }
            R.id.btnJenis ->{
                var intent = Intent(this, JenisActivity::class.java)
                startActivity(intent)
            }
        }
    }
}